#!/bin/bash

LRED='\033[1;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

pip install requests
pip uninstall pip-system-certs &> /dev/null

echo -e "${CYAN}start webserver with self-signed cert${NC}"
python3 test/simple-https-server.py &

echo -e "${CYAN}Should fail to verify self-signed cert${NC}"
curl -s https://localhost:4443 || (echo -e "\n${LRED}curl failed as expected${NC}"; true)
python3 test/request.py 2> /dev/null || (echo -e "${LRED}python failed as expected${NC}\n"; true)

echo -e "${CYAN}Install self-signed cert in system, curl should pass${NC}"
cp test/pki/ca.crt  /usr/local/share/ca-certificates/
update-ca-certificates

curl -s https://localhost:4443 > /dev/null && echo -e "\n${GREEN}curl passed as expected${NC}"
python3 test/request.py 2> /dev/null || (echo -e "${LRED}python failed as expected${NC}\n"; true)

echo -e "${CYAN}Install pip-system-certs, curl & python should pass${NC}"
pip install .

curl -s https://localhost:4443 > /dev/null && echo -e "\n${GREEN}curl passed as expected${NC}"
python3 test/request.py > /dev/null && echo -e "${GREEN}python passed as expected${NC}\n"

echo -e "${CYAN}Uninstall pip-system-certs, python should fail again${NC}"
pip uninstall pip-system-certs &> /dev/null

curl -s https://localhost:4443 > /dev/null && echo -e "\n${GREEN}curl passed as expected${NC}"
python3 test/request.py 2> /dev/null || (echo -e "${LRED}python failed as expected${NC}\n"; true)

echo -e "${CYAN}Install pip-system-certs from wheel, curl & python should pass${NC}"
python setup.py bdist_wheel
pip install dist/*.whl

curl -s https://localhost:4443 > /dev/null && echo -e "\n${GREEN}curl passed as expected${NC}"
python3 test/request.py > /dev/null && echo -e "${GREEN}python passed as expected${NC}\n"

